use super::components::synth_module::{SynthModule, UISpec};
use serde_json::from_str;
use yew::prelude::*;
use web_sys::{MouseEvent, console};

pub struct App {
    props: Props,
    link: ComponentLink<Self>,
}

pub enum Msg {
    GlobalDrag(MouseEvent),
    GlobalDragEnd(MouseEvent),
}

#[derive(Properties, Clone, Default)]
pub struct Props {
    pub synth_modules: ChildrenWithProps<SynthModule>,
    #[prop_or(None)]
    pub last_drag: Option<(f64, f64)>,
    pub drag_start: Option<(f64, f64)>,
}

impl Component for App {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        App { props, link }
    }

    fn view(&self) -> Html {
        let data = r#"
            {
                "width": 22,
                "height": 12,
                "name": "ADSR Envelope",
                "knobs": [
                    {
                        "description": "Attack"
                    },
                    {
                        "description": "Decay",
                        "left": 5
                    },
                    {
                        "description": "Sustain",
                        "left": 10
                    },
                    {
                        "description": "Release",
                        "left": 15
                    }
                ]
            }"#;
        let spec: Result<UISpec, serde_json::Error> = from_str(data);
        let spec_with_message: Result<UISpec, String> = match spec {
            Ok(s) => Ok(s),
            Err(err) => Err(format!("Error in UI description: {}", err)),
        };
        console::log_1(&format!("dragging {:?}", self.props.last_drag).into());
        html! {
            <div onmousemove=self.link.callback(|e| Msg::GlobalDrag(e))
                onmouseup=self.link.callback(|e| Msg::GlobalDragEnd(e))
                style="position: absolute; top: 0; right: 0; bottom: 0; left: 0;">
                <SynthModule ui_spec=spec_with_message last_drag=self.props.last_drag/>
            </div>
        }
    }

    fn update(&mut self, msg: Msg) -> ShouldRender {
        match msg {
            // Propagate global drag event to all children
            Msg::GlobalDrag(event) => {
                self.props.last_drag = Some((event.client_x() as f64, event.client_y() as f64));
                true
            }
            Msg::GlobalDragEnd(_) => {
                console::log_1(&"global drag end".into());
                self.props.last_drag = None;
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }
}
