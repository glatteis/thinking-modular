use super::knob::Knob;
use serde_derive::{Deserialize, Serialize};
use yew::prelude::*;
use web_sys::{MouseEvent, console};

#[derive(Clone, PartialEq, Default, Deserialize, Serialize)]
pub struct UISpec {
    pub knobs: Vec<super::knob::ConfigurableProps>,
    pub width: f64,
    pub height: f64,
    pub name: String,
}

pub struct SynthModule {
    props: Props,
    link: ComponentLink<Self>,
    drag_start: Option<MouseEvent>,
}

pub enum Msg {
    DragStart(MouseEvent),
    Dragged(Option<(f64, f64)>),
}

#[derive(Properties, Clone, PartialEq, Serialize, Deserialize)]
pub struct Props {
    /// Contains either the configuration or the error message if the configuration was invalid.
    pub ui_spec: Result<UISpec, String>,

    /// contains the start of the dragging when dragging is happening, else is none
    #[prop_or(None)]
    pub last_drag: Option<(f64, f64)>,
    #[prop_or(0.0)]
    pub top: f64,
    #[prop_or(0.0)]
    pub left: f64,
}

impl Component for SynthModule {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        SynthModule {
            props,
            link,
            drag_start: None,
        }
    }

    fn view(&self) -> Html {
        match self.props.ui_spec.as_ref() {
            Ok(spec) => {
                html! {
                    <div class="absolute border border-gray-600 rounded"
                        style={ format!(
                            "width: {}rem; height: {}rem; top: {}px; left: {}px;",
                            spec.width, spec.height, self.props.top, self.props.left) }
                            onmousedown=self.link.callback(|e| Msg::DragStart(e))>
                        <span class="absolute text-xl" style="top: 1rem; left: 1rem;">
                            { &spec.name }
                        </span>
                        <div class="absolute" style="top: 3rem; left: 1rem;">
                            {
                                html! {
                                    {
                                        for spec.knobs.iter().map(|knob_props| {
                                            html! {
                                                <Knob conf=knob_props/>
                                            }
                                        })
                                    }
                                }
                            }
                        </div>
                    </div>
                }
            }
            Err(message) => {
                html! {
                    <div class="absolute border border-gray-600 rounded"
                            style={ format!("width: {}rem; height: {}rem;", 20, 5) }>
                        <span style="color: red;">
                            { message }
                        </span>
                    </div>
                }
            }
        }
    }

    fn update(&mut self, msg: Msg) -> ShouldRender {
        match msg {
            Msg::DragStart(event) => {
                self.drag_start = Some(event);
                console::log_1(&format!("drag_start {:?} at {:?}", self.drag_start, self.props.ui_spec.as_ref().unwrap().name).into());
                true
            },
            Msg::Dragged(event) => {
                console::log_1(&format!("module_dragged {:?}", self.drag_start).into());
                match &self.drag_start {
                    None => false,
                    Some(drag_start) => {
                        match event {
                            None => {
                                self.drag_start = None;
                                console::log_1(&"setting drag_start back to None".into());
                                false
                            },
                            Some(event) => {
                                console::log_1(&format!("dragging event {}, {}", &event.0, &event.1).into());
                                let drag_start_coords = (drag_start.client_x() as f64, drag_start.client_y() as f64);
                                self.props.left = event.0 as f64 - drag_start_coords.0 as f64;
                                self.props.top = event.1 as f64 - drag_start_coords.1 as f64;
                                console::log_1(&format!("set coords to {}, {}", self.props.left, self.props.top).into());
                                true
                            }
                        }
                    }
                }
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            if props.last_drag == None {
                self.drag_start = None;
            } else if props.last_drag != self.props.last_drag && self.drag_start != None {
                self.link.send_message(Msg::Dragged(props.last_drag));
            }
            self.props = props;
            true
        } else {
            false
        }
    }
}
