use serde_derive::{Deserialize, Serialize};
use web_sys::WheelEvent;
use yew::prelude::*;

pub struct Knob {
    props: Props,
    link: ComponentLink<Self>,
}

pub enum Msg {
    // StartRotating,
    RotatingWheel(f64),
    // StopRotating
}

// Defaults for the knob props. Needs to be done because serde macros are lacking
fn default_max() -> Option<f64> {
    Some(150.0)
}
fn default_min() -> Option<f64> {
    Some(-150.0)
}
fn default_scale() -> f64 {
    1.0
}
fn default_background_color() -> String {
    String::from("#f7fafc")
}

#[derive(Clone, PartialEq, Serialize, Deserialize)]
pub struct ConfigurableProps {
    #[serde(default)]
    pub value: f64,

    pub description: String,

    #[serde(default = "default_max")]
    pub max: Option<f64>,
    #[serde(default = "default_min")]
    pub min: Option<f64>,

    #[serde(default)]
    pub top: f64,
    #[serde(default)]
    pub left: f64,

    #[serde(default = "default_scale")]
    pub scale: f64,

    #[serde(default = "default_background_color")]
    pub background_color: String,
}

#[derive(Properties, Clone, PartialEq, Serialize, Deserialize)]
pub struct Props {
    pub conf: ConfigurableProps,
}

impl Component for Knob {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Knob { props, link }
    }

    fn view(&self) -> Html {
        let wheel_event = self
            .link
            .callback(|e: WheelEvent| Msg::RotatingWheel(e.delta_y()));
        html! {
            <div class="text-center w-16 h-30 absolute"
                    style={ format!("top: {}rem; left: {}rem; transform: scale({});", self.props.conf.top, self.props.conf.left, self.props.conf.scale) }
                    // onmousedown=self.link.callback(|_| Msg::RotatingWheel)
                    onmousewheel=wheel_event>
                <div class="rounded-full h-16 w-16 items-center justify-center border border-black"
                        style={ format!("transform: rotate({}deg); background-color: {};", self.props.conf.value, self.props.conf.background_color) } >
                    <div class="relative h-5 w-1 bg-black" style="left: calc(50% - 0.125rem)">
                    </div>
                </div>
                <p> { &self.props.conf.description } </p>
            </div>
        }
    }

    fn update(&mut self, msg: Msg) -> ShouldRender {
        match msg {
            Msg::RotatingWheel(delta) => {
                // For consistency across browsers
                if delta > 0.0 {
                    self.props.conf.value += 10.0;
                } else if delta < 0.0 {
                    self.props.conf.value -= 10.0;
                }
                if self.props.conf.max.is_some()
                    && self.props.conf.max.unwrap() < self.props.conf.value
                {
                    self.props.conf.value = self.props.conf.max.unwrap();
                }
                if self.props.conf.min.is_some()
                    && self.props.conf.min.unwrap() > self.props.conf.value
                {
                    self.props.conf.value = self.props.conf.min.unwrap();
                }
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }
}
